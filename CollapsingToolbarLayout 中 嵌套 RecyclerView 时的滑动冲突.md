# CollapsingToolbarLayout 中 嵌套 RecyclerView 时的滑动冲突

## 1、问题描述

某个页面的布局层级如下：

```xml
<CoordinatorLayout
    <AppBarLayout
        <CollapsingToolbarLayout
            <LinearLayout
                <RecyclerView

                </RecyclerView>
            </LinearLayout>
        </CollapsingToolbarLayout>
    </AppBarLayout>
    <RecyclerView
    </RecyclerView>
</CoordinatorLayout>
```

CollapsingToolbarLayout 嵌套了 RecyclerView，后者的高度是固定的，当列表的 Item 数目较多时，可以滑动浏览。但这样嵌套使用后，会首先响应外部 RecyclerView 的滑动，导致 CollapsingToolbarLayout 中的 RecyclerView 无法滑动浏览。

## 2、解决方案

可自定义一个 RecycleView：

```kotlin
class NestedScrollRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RecyclerView(context, attrs, defStyle) {

    init {
       isNestedScrollingEnabled = false
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        parent.requestDisallowInterceptTouchEvent(true)
        return super.dispatchTouchEvent(ev)
    }
}
```

这里主要做了两点处理：

* `parent.requestDisallowInterceptTouchEvent(true)` 请求父布局不再拦截其触摸事件；
* `isNestedScrollingEnabled = false` ，嵌套时不再随着父布局滑动。CollapsingToolbarLayout 中 嵌套 RecyclerView 时的滑动冲突
